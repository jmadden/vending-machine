import java.util.ArrayList;
import java.util.HashMap;
public class VendingMachine {
	float balance;
	ArrayList<Transaction> transHist = new ArrayList<Transaction>(); //Transaction History
	
	VendingMachine(float balance){
		this.balance = balance;
	}
	
	public void processTransaction(Transaction transaction) {
		transHist.add(transaction);
			balance+=transaction.count*transaction.item.sellPrice;
		
	}
	public void printTransaction() {
		for(int i = 0; i < transHist.size(); i++) {
				String value = Float.toString((transHist.get(i).item.sellPrice*transHist.get(i).count));
				String count = Float.toString(transHist.get(i).count);
				System.out.println(transHist.get(i).item.name + " $" + value + " X" + count);
		}
		System.out.println(balance);
	}
	
	public static void main(String[] args) {
		String alphaBeta= "abcdefghijklmnopqrstuvwxyz";
		VendingMachine vm = new VendingMachine(0);
		ArrayList<Item> items= new ArrayList<Item>();
		ArrayList<Transaction> transactions= new ArrayList<Transaction>();
		for(int i = 0; i < Math.ceil(Math.random()*20)+5; i++) {
			String itemName = "";
			for(int q = 0; q < Math.ceil(Math.random()*5)+3; q++) {
				itemName = itemName+alphaBeta.charAt((int)Math.floor(Math.random()*26));
			}
			float cost = (float)(Math.floor(Math.random()*10)+1);
			items.add(new Item(cost,itemName));
		}
		for(int k = 0; k < items.size(); k++) {
			transactions.add(new Transaction((int)(Math.ceil(Math.random()*20)+1), items.get(k)));
		}
		for(int j = 0; j < transactions.size(); j++) {
			vm.processTransaction(transactions.get(j));
		}
		vm.printTransaction();
	}
}
